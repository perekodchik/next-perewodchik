import classNames from "classnames";
import { HTMLProps } from "react";

type ButtonVariant = "primary" | "secondary";

type Props = HTMLProps<HTMLButtonElement> & {
  variant?: ButtonVariant;
};

export const Button = ({ variant = "primary", ...props }: Props) => {
  const className = classNames(
    "h-16 w-full outline-none rounded-lg uppercase text-lg active:opacity-70 border border-solid border-black",
    [...(variant === "secondary" ? ["hover:bg-gray-200"] : [])],
    [
      ...(variant === "primary"
        ? ["bg-sky-500", "hover:bg-sky-400", "text-white"]
        : []),
    ]
  );

  return <button {...props} type="button" className={className} />;
};
