import { ReactNode } from "react";
import { Footer } from "../footer";

type Props = {
  children: ReactNode;
};

export const Layout = ({ children }: Props) => {
  return (
    <div className={"flex flex-col w-full items-center"}>
      {/* <nav>nav bar</nav> */}
      <main className={"flex flex-col items-center w-full max-w-7xl px-4"}>
        {children}
      </main>
      <Footer />
    </div>
  );
};
