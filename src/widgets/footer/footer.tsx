import Image from "next/image";
import { FooterCopyright } from "./footer-copyright";

import styles from "./style.module.css";

export const Footer = () => {
  return (
    <footer
      className={
        "flex items-center justify-center pb-4 mt-48 w-full gap-6 relative"
      }
    >
      <Image
        src="/images/telegram-logo.webp"
        width={64}
        height={64}
        alt="telegram link"
      />
      <Image
        src="/images/instagram-logo.png"
        width={64}
        height={64}
        alt="instagram link"
      />
      <Image
        src="/images/linkedin-logo.png"
        width={64}
        height={64}
        alt="linkedin link"
      />
      <Image
        src="/images/gitlab-logo.png"
        width={64}
        height={64}
        alt="gitlab link"
      />
      <FooterCopyright />
    </footer>
  );
};
