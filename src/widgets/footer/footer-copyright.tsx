export const FooterCopyright = () => {
  return (
    <div className={"absolute bottom-4 right-4"}>
      <p className="text-gray-500">© 2023 perewodchik</p>
    </div>
  );
};
