import { Example } from "@/entities/three";
import { Layout } from "@/widgets/layout";
import Head from "next/head";

export default function Three() {
  return (
    <>
      <Head>
        <title>Three.js playground</title>
        <meta
          name="description"
          content="Some examples while learning three.js"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="author" content="perewodchik" />
      </Head>
      <Example />
    </>
  );
}
