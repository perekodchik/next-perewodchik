import { ReactNode, useState } from "react";
import { Canvas } from "@react-three/fiber";
import {
  useGLTF,
  OrbitControls,
  Environment,
  Stats,
  Html,
} from "@react-three/drei";
import { useControls } from "leva";

const Models: Record<string, string> = {
  hammer: "/models/hammer.glb",
  drill: "/models/drill.glb",
  tapeMeasure: "/models/tapeMeasure.glb",
};

function Model({ url }: { url: string }) {
  const { scene } = useGLTF(url);
  const [cache, setCache] = useState<any>({});

  if (!cache[url]) {
    const annotations: ReactNode[] = [];

    scene.traverse((o) => {
      if (o.userData.prop) {
        annotations.push(
          <Html
            key={o.uuid}
            position={[o.position.x, o.position.y, o.position.z]}
            distanceFactor={0.25}
          >
            <div className="p-4 rounded-md bg-slate-800 text-slate-100 text-2xl">
              {o.userData.prop}
            </div>
          </Html>
        );
      }
    });

    setCache({
      ...cache,
      [url]: <primitive object={scene}>{annotations}</primitive>,
    });
  }
  return cache[url];
}

export default function App() {
  const { model } = useControls({
    model: {
      value: "hammer",
      options: Object.keys(Models),
    },
  });

  return (
    <div className="h-screen bg-slate-800">
      <Canvas camera={{ position: [0, 0, -0.2], near: 0.025 }}>
        <Environment preset="warehouse" background />
        <group>
          <Model url={Models[model]} />
        </group>
        <OrbitControls autoRotate />
        <Stats />
      </Canvas>
      <span id="info">
        The {model.replace(/([A-Z])/g, " $1").toLowerCase()} is selected.
      </span>
    </div>
  );
}
