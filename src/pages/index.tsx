import { AboutMe, HeroBanner, SkillsConnector } from "@/entities/home";
import { Layout } from "@/widgets/layout";
import Head from "next/head";
import { ReactElement } from "react";

export default function Home() {
  return (
    <>
      <Head>
        <title>Frontend development by perewodchik</title>
        <meta
          name="description"
          content="I am a React developer with experience in building web applications."
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="keywords"
          content="React, web development, front-end, developer"
        />
        <meta name="author" content="perewodchik" />
      </Head>
      <HeroBanner />
      <AboutMe />
      <SkillsConnector />
    </>
  );
}

Home.getLayout = function getLayout(page: ReactElement) {
  return <Layout>{page}</Layout>;
};
