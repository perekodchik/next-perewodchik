import { MeshProps, useLoader } from "@react-three/fiber";
import { folder, useControls } from "leva";
import { useMemo, useRef } from "react";
import { BufferGeometry, Mesh, MeshToonMaterial, TextureLoader } from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

type Props = MeshProps;

export const Model = ({ name, ...props }: Props) => {
  const ref = useRef<Mesh>(null);

  const gltf = useLoader(GLTFLoader, "/models/monkey.glb");

  const options = useMemo(() => {
    return {
      rotation: folder({
        rotationX: { value: 0, min: 0, max: Math.PI * 2, step: 0.01 },
        rotationY: { value: 0, min: 0, max: Math.PI * 2, step: 0.01 },
        rotationZ: { value: 0, min: 0, max: Math.PI * 2, step: 0.01 },
      }),
      position: folder({
        positionX: { value: 0, min: -5, max: 5, step: 0.01 },
        positionY: { value: 0, min: -5, max: 5, step: 0.01 },
        positionZ: { value: 0, min: -5, max: 5, step: 0.01 },
      }),
      scale: folder({
        scaleX: 1,
        scaleY: 1,
        scaleZ: 1,
      }),

      color: { value: "cyan" },
    };
  }, []);

  const controls = useControls(name ?? "Object", options);

  return (
    <mesh
      ref={ref}
      {...props}
      rotation={[controls.rotationY, controls.rotationY, controls.rotationZ]}
      position={[controls.positionX, controls.positionY, controls.positionZ]}
      scale={[controls.scaleX, controls.scaleY, controls.scaleZ]}
      castShadow
      receiveShadow
    >
      <primitive
        object={gltf.scene}
        position={[0, 1, 0]}
        children-0-castShadow
      />
    </mesh>
  );
};
