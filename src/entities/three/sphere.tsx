import { MeshProps } from "@react-three/fiber";
import { useControls } from "leva";
import { useMemo, useRef } from "react";
import { BufferGeometry, Mesh, MeshToonMaterial } from "three";

type Props = MeshProps;

export const Sphere = ({ name, ...props }: Props) => {
  const ref = useRef<Mesh>(null);

  const options = useMemo(() => {
    return {
      rotationX: { value: 0, min: 0, max: Math.PI * 2, step: 0.01 },
      rotationY: { value: 0, min: 0, max: Math.PI * 2, step: 0.01 },
      rotationZ: { value: 0, min: 0, max: Math.PI * 2, step: 0.01 },
      positionX: { value: 0, min: -5, max: 5, step: 0.01 },
      positionY: { value: 0, min: -5, max: 5, step: 0.01 },
      positionZ: { value: 0, min: -5, max: 5, step: 0.01 },
      color: { value: "cyan" },
    };
  }, []);

  const controls = useControls(name ?? "Box", options);

  return (
    <mesh
      ref={ref}
      {...props}
      rotation={[controls.rotationX, controls.rotationY, controls.rotationZ]}
      position={[controls.positionX, controls.positionY, controls.positionZ]}
      castShadow
      receiveShadow
    >
      <sphereGeometry />
      <meshStandardMaterial color={controls.color} />
    </mesh>
  );
};
