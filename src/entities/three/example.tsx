import { Environment, OrbitControls, Stats } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import { Box } from "./box";
import { Floor } from "./floor";
import { Lights } from "./lights";
import { Model } from "./model";
import { Sphere } from "./sphere";

export const Example = () => {
  return (
    <div className="h-screen bg-slate-800">
      <Canvas shadows>
        <Lights />
        <Environment files={"/environments/envirnment.hdr"} background />
        <OrbitControls />
        <axesHelper args={[5]} />
        <gridHelper args={[20, 20, 0xff0000, "teal"]} />
        <Box name="Box 1" />
        <Sphere name="Sphere" />
        <Floor />
        <Stats />
        <Model />
      </Canvas>
    </div>
  );
};
