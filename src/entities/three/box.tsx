import { MeshProps, useLoader } from "@react-three/fiber";
import { folder, useControls } from "leva";
import { useMemo, useRef } from "react";
import { BufferGeometry, Mesh, MeshToonMaterial, TextureLoader } from "three";

type Props = MeshProps;

export const Box = ({ name, ...props }: Props) => {
  const ref = useRef<Mesh>(null);

  const texture = useLoader(TextureLoader, "/images/front-ops.jpeg");

  const options = useMemo(() => {
    return {
      rotation: folder({
        rotationX: { value: 0, min: 0, max: Math.PI * 2, step: 0.01 },
        rotationY: { value: 0, min: 0, max: Math.PI * 2, step: 0.01 },
        rotationZ: { value: 0, min: 0, max: Math.PI * 2, step: 0.01 },
      }),
      position: folder({
        positionX: { value: 0, min: -5, max: 5, step: 0.01 },
        positionY: { value: 0, min: -5, max: 5, step: 0.01 },
        positionZ: { value: 0, min: -5, max: 5, step: 0.01 },
      }),
      scale: folder({
        scaleX: 1,
        scaleY: 1,
        scaleZ: 1,
      }),

      color: { value: "cyan" },
    };
  }, []);

  const controls = useControls(name ?? "Object", options);

  return (
    <mesh
      ref={ref}
      {...props}
      rotation={[controls.rotationY, controls.rotationY, controls.rotationZ]}
      position={[controls.positionX, controls.positionY, controls.positionZ]}
      scale={[controls.scaleX, controls.scaleY, controls.scaleZ]}
      castShadow
      receiveShadow
    >
      <boxGeometry></boxGeometry>
      <meshStandardMaterial map={texture} color={controls.color} />
    </mesh>
  );
};
