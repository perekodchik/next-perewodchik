import React from "react";
import { otherSkills, skills } from "../constants";
import { Skills } from "../organisms";

export const SkillsConnector = () => {
  const selectedSkill = null;

  const handleSkillSelect = (title: string) => null;

  return (
    <Skills
      skills={skills}
      otherSkills={otherSkills}
      selectedSkill={selectedSkill}
      onSkillClick={handleSkillSelect}
    />
  );
};
