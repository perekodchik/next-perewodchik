export type SkillParams = {
  imgSrc: string;
  title: string;
};
