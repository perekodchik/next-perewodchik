import { SkillParams } from "./types";

export const skills: SkillParams[] = [
  {
    imgSrc: "/images/react-logo.png",
    title: "react",
  },
  {
    imgSrc: "/images/typescript-logo.png",
    title: "typescript",
  },
  {
    imgSrc: "/images/nextjs-logo.png",
    title: "nextJS",
  },
];

export const otherSkills = [
  "SEO",
  "Front-Ops",
  "Storyook",
  "Styled Components",
  "GraphQL",
  "React-Query",
  "React-Native",
  "SEO",
  "Redux",
  "Effector",
  "Atomic-Design",
  "FSD",
  "TailwindCSS",
  "Three.js",
];
