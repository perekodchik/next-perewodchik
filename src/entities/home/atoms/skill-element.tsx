import classNames from "classnames";
import Image from "next/image";
import React from "react";

type Props = {
  imgSrc: string;
  title: string;
  selected?: boolean;
  onClick: () => void;
};

export const SkillElement = ({ imgSrc, title, selected, onClick }: Props) => {
  const titleClassName = classNames("ml-2", {
    "text-sky-400": selected,
  });

  return (
    <div
      className="flex items-center p-2 w-full rounded-md hover:bg-slate-100 cursor-pointer"
      onClick={onClick}
    >
      <Image width={32} height={32} src={imgSrc} alt={title} />
      <p className={titleClassName}>{title}</p>
    </div>
  );
};
