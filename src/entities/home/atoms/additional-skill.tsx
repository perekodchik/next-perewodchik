import React from "react";

type Props = {
  title: string;
};

export const AdditionalSkill = ({ title }: Props) => {
  return <p className="text-base w-full text-center text-slate-600">{title}</p>;
};
