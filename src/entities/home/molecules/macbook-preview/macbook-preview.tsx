import { ContactShadows, Environment, OrbitControls } from "@react-three/drei";
import { Canvas, useFrame, useThree } from "@react-three/fiber";
import React, { Suspense } from "react";
import { LaptopModel } from "../../atoms";

type Props = {};

export const MacBookPreview = ({}: Props) => {
  return (
    <Canvas camera={{ position: [-2, 7, -12], fov: 70 }}>
      <directionalLight position={[10, 10, 10]} intensity={1.5} />
      <Suspense fallback={null}>
        <group rotation={[0, Math.PI, 0]} position={[0, 1, 0]}>
          <LaptopModel position={[0, -1, 0]} />
        </group>
        <Environment preset="city" />
      </Suspense>
      <OrbitControls
        enableDamping
        enablePan={false}
        maxDistance={15}
        minAzimuthAngle={(3 * Math.PI) / 4}
        maxAzimuthAngle={(5 * Math.PI) / 4}
        minPolarAngle={Math.PI / 4}
        maxPolarAngle={Math.PI / 2}
      />
    </Canvas>
  );
};
