import { SkillElement } from "../../atoms/skill-element";
import { SkillParams } from "../../types";

type Props = {
  skills: SkillParams[];
  selectedSkill: string | null;
  onSkillClick: (title: string) => void;
};

export const SkillsGroup = ({ skills, selectedSkill, onSkillClick }: Props) => {
  return (
    <section className="flex flex-col w-full">
      <h2 className="w-full">My primary skills</h2>
      <p className="text-xs text-slate-400 mb-4">
        Select a skill to show additional info
      </p>
      {skills.map(({ imgSrc, title }) => (
        <SkillElement
          key={title}
          imgSrc={imgSrc}
          title={title}
          selected={title === selectedSkill}
          onClick={() => onSkillClick(title)}
        />
      ))}
      <h3 className="mt-4 mb-3">Other skills</h3>
      <div className="flex flex-wrap flex-row w-full"></div>
    </section>
  );
};
