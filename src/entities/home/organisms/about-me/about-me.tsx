import Image from "next/image";

export const AboutMe = () => {
  return (
    <section className="flex flex-col gap-4 items-center w-full justify-center md:flex-row">
      <Image
        src="/images/perewodchik-about.jpg"
        alt="perewodchik about image"
        width={400}
        height={500}
      />
      <p>
        Greetings! My name is Vladislav Smirnov, and I am a frontend developer
        with a focus on crafting high-quality web applications that offer an
        exceptional user experience. With expertise in React and TypeScript, as
        well as experience in FrontOps and SEO, I possess a comprehensive
        understanding of the modern web development landscape. I am based in the
        beautiful country of Montenegro, where I enjoy immersing myself in the
        local culture and exploring the great outdoors. As a proficient English
        speaker at a B2 level, I am able to communicate effectively with
        international clients. I am excited to offer my skills and expertise to
        help bring your web project to life. Thank you for considering my
        services.
      </p>
    </section>
  );
};
