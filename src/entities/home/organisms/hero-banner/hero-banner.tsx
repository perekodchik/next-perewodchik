import { Button } from "@/shared/core";

export const HeroBanner = () => {
  return (
    <section className="flex flex-col gap-2 items-center justify-center w-full min-h-screen">
      <h1 className="text-center max-w-6xl">
        Innovative Front-End Development for Your Digital Presence
      </h1>
      <h2 className="text-center text-gray-500 max-w-4xl">
        As a front-end developer, I specialize in creating engaging websites
        that showcase your brand and captivate your audience.
      </h2>
      <div className="flex flex-col md:flex-row gap-2 items-center mt-2 w-full md:w-[400px]">
        <Button variant="secondary">Read Blog</Button>
        <Button>Contact me</Button>
      </div>
    </section>
  );
};
