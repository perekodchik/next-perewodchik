import React from "react";
import { AdditionalSkill } from "../../atoms";
import { MacBookPreview } from "../../molecules/macbook-preview";
import { SkillsGroup } from "../../molecules/skills-group/skills-group";
import { SkillParams } from "../../types";

type Props = {
  skills: SkillParams[];
  otherSkills: string[];
  selectedSkill: string | null;
  onSkillClick: (title: string) => void;
};

export const Skills = ({
  skills,
  selectedSkill,
  otherSkills,
  onSkillClick,
}: Props) => {
  return (
    <section className="flex flex-col md:flex-row gap-4 w-full mt-32">
      <div className="w-full md:w-2/5">
        <SkillsGroup
          skills={skills}
          onSkillClick={onSkillClick}
          selectedSkill={selectedSkill}
        />
        <div className="grid span grid-cols-3 auto-rows-fr	items-center gap-1">
          {otherSkills.map((skill) => (
            <AdditionalSkill key={skill} title={skill} />
          ))}
        </div>
      </div>
      <div className="w-full md:w-3/5 h-[600px]">
        <MacBookPreview />
      </div>
    </section>
  );
};
